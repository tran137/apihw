﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace APIHW.Models
{
    public partial class WordItems
    {
        [JsonProperty("type")]
        public String type { get; set; }

        [JsonProperty("definition")]
        public String definition { get; set; }

        [JsonProperty("example")]
        public String example { get; set; }
    }

    public partial class WordItems
    {
        public static WordItems FromJson(string json) => JsonConvert.DeserializeObject<WordItems>(json, APIHW.Models.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this WordItems self) => JsonConvert.SerializeObject(self, APIHW.Models.Converter.Settings);
    }

    internal class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            }
        };
    }
}
