﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Net.Http;
using ModernHttpClient;
using APIHW.Models;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using Plugin.Connectivity;

namespace APIHW
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        async void Handle_Search(object sender, System.EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected == false)
            {
                await DisplayAlert("Alert", "No internet connection", "OK");
            }
            else
            {
                var client = new HttpClient(new NativeMessageHandler());

                var uri = new Uri(string.Format("https://owlbot.info/api/v2/dictionary/" + SearchWord.Text));

                var request = new HttpRequestMessage();
                request.Method = HttpMethod.Get;
                request.RequestUri = uri;
                request.Headers.Add("Application", "application / json");

                HttpResponseMessage response = await client.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    IEnumerable<WordItems> result = JsonConvert.DeserializeObject<IEnumerable<WordItems>>(content);
                    WordItems[] array = result.Cast<WordItems>().ToArray();

                    await Navigation.PushAsync(new DefinitionPage(array, SearchWord.Text));

                }
            }
                
        }
    }
}
