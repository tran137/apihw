﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using APIHW.Models;

namespace APIHW
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DefinitionPage : ContentPage
	{
		public DefinitionPage (WordItems[] array , String word)
		{
			InitializeComponent ();

            Word.Text = word.ToLower();

            for (int i = 0; i < array.Length; i++)
            {
                Definitions.Text = Definitions.Text + $"{i + 1}. ";
                Definitions.Text = Definitions.Text + "Type: " + array[i].type + "\n";
                Definitions.Text = Definitions.Text + "Definition: " + array[i].definition + "\n";
                Definitions.Text = Definitions.Text + "Example: " + array[i].example + "\n\n";
            }
        }
	}
}